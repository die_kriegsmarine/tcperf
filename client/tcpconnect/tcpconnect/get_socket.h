/*
 * $Id: getSocket.h,v 1.1 2008/11/23 12:18:50 ykishi Exp ykishi $
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>

#ifdef __cplusplus
extern "C" {
#endif

	int getSocket (const char *hostname, int portNumber);
	
#ifdef __cplusplus
}
#endif


