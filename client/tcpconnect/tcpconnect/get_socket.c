#include "get_socket.h"
#include "set_nonblocking.h"
#include <errno.h>

int
getSocket (const char *hostname, int portNumber ) {

    struct sockaddr_in addr;
    struct hostent *he;
    int sock;

    if ( hostname == NULL ) {
        perror ( "host is undefined!" );
        exit ( 1 );
    }

    if ( ( sock = socket ( AF_INET, SOCK_STREAM, 0 ) ) < 0 ) {
        perror ( "socket" );
        exit ( 2 );
    }

    bzero ( ( char * ) &addr, sizeof ( addr ) );

    if ( ( he = gethostbyname ( hostname ) ) == NULL ) {
        perror ( "No such host" );
        exit ( 3 );
    }
    bcopy ( he->h_addr, &addr.sin_addr, he->h_length );
    addr.sin_family = AF_INET;
    addr.sin_port = htons ( portNumber );

		setNonblocking(sock);

		int cresult = connect ( sock, ( struct sockaddr * ) &addr, sizeof ( addr ) ); 
    if (cresult  < 0 && errno != EINPROGRESS) {
        perror ( "connect" );
			fprintf(stderr, "%d\n", errno);
        exit ( 4 );
    }

    return sock;
}

