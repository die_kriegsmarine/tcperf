///* For sockaddr_in */
#include <netinet/in.h>
///* For socket functions */
#include <sys/socket.h>
///* For fcntl */
#include <fcntl.h>
///* for select */
#include <sys/select.h>

#include <stdlib.h>
#include <stdio.h>
#include "get_socket.h"


int TOTAL_CONNECTED = 0;

void
make_nonblocking(int fd) {
	fcntl(fd, F_SETFL, O_NONBLOCK);
}

int
main(int argc, const char* argv[]) {
	printf("FD_SETSIZE=%d\n", FD_SETSIZE);
	setvbuf(stdout, NULL, _IONBF, 0);
	
	const char *host = argv[1];
	int port = atoi(argv[2]);
	int maxClients = atoi(argv[3]);
	
	for (int i = 0; i < maxClients; i++ ) {
		int fd = getSocket ( host, port );
		fprintf ( stderr, "... socket handle [%d] created ... \n", fd );
	}

	getchar();
	
	return 0;
}
