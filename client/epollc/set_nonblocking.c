#include "set_nonblocking.h"

/**
* $Source: /home/ykishi/C-test/epoll-client/RCS/setNonblocking.c,v $
*/

void
setNonblocking ( int sock ) {
    int flag = fcntl ( sock, F_GETFL, 0 );
    fcntl ( sock, F_SETFL, flag | O_NONBLOCK );
}
