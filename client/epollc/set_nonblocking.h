#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/epoll.h>
#include <netinet/in.h>

/**
* $Source: /home/ykishi/C-test/epoll-client/RCS/setNonblocking.h,v $
*/

#ifdef __cplusplus
extern "C" {
#endif

	void setNonblocking (int sock);
	
#ifdef __cplusplus
}
#endif



