#define _POSIX_C_SOURCE 199309 
#include <time.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/epoll.h>
#include <netinet/in.h>
#include "get_socket.h"
#include "set_nonblocking.h"
#include <assert.h>
#include <map>
#include <sys/time.h>
#include <errno.h>
#include <sys/time.h>
#include <signal.h>


bool RESPAWN_CLIENTS = false;
bool DONE = false;

namespace {
	time_t timeval_subtract(struct timeval *t2, struct timeval *t1) {
	    return (t2->tv_usec + 1000000 * t2->tv_sec) - (t1->tv_usec + 1000000 * t1->tv_sec);
	}	
}

void sigint_handler(int) {
	printf("SIGINT handled\n");
	DONE = true;
}

struct _client {
	int						fd;
	unsigned int 	sent;
	unsigned int 	recv;
	bool					ready;
};


bool new_client(const char* host, int port, epoll_event* ev, int epfd) {
  int fd = getSocket ( host, port );	                									
  memset(ev, 0, sizeof(*ev));
  ev->events = EPOLLIN | EPOLLOUT | EPOLLET;
  ev->data.fd = fd;
  if (epoll_ctl(epfd, EPOLL_CTL_ADD, fd, ev) < 0 ){
      fprintf ( stderr, "epoll_ctl()\n" );
			return false;
  }
	return true;	
}

int
main ( int argc, char **argv ) {

    char *host;
    int port;
    char *requestURI;
    int i = 0;
    int maxClients = 0;
		int requests = 1;
		int running = 0;
		
		typedef std::map<int, _client> clients_t;
		clients_t clients;

    if ( argc != 10 ) {
        fprintf ( stderr, "Usage: %s <host> <port> <requestURI> <maxClients> <requests> <verbose> <conn_only> <grace_conn> <readch_after_conn>\n", argv[0] );
        return -1;
    }
    host = argv[1];
    port = atoi ( argv[2] );
    requestURI = argv[3];
    maxClients = atoi(argv[4]);
		requests = atoi(argv[5]);
		bool verbose = atoi(argv[6]) == 1;
		bool conn_only = atoi(argv[7]) == 1;
		bool grace_conn = atoi(argv[8]) == 1;
		bool reach_afer_conn = atoi(argv[9]) == 1;

    //--------------------------------------------------
    // create epoll's descriptors
    //--------------------------------------------------
    int epfd;
    if ( ( epfd = epoll_create ( maxClients ) ) < 0 ) {
        perror ( "epoll_create" );
        exit ( 1 );
    }


		sigset(SIGINT, sigint_handler);

		struct timeval tv1, tv2;
		int seed = getpid() + getppid();
		gettimeofday(&tv1, 0);
		seed += tv1.tv_sec + tv1.tv_usec % 100000;
		srandom(seed);
		if (grace_conn) {
			useconds_t sleep_usec = random() % 10000000;		
			fprintf(stderr, "sleeping for %d usec\n", sleep_usec);
			usleep(sleep_usec);
		}
		
		// useconds_t stagger = 10000000 / maxClients;

    //--------------------------------------------------
    // create as many socket handles as needed
    //--------------------------------------------------
    struct _client* sock = (_client*)malloc(sizeof(struct _client)*maxClients);

		gettimeofday(&tv1, 0);
		//seems that 10 msec is the lowest resolution possible on our linux system
		timespec for40ms = {0,40000000};

    for ( i = 0; i < maxClients; i++ ) {
        sock[i].fd = getSocket ( host, port );
				// sock[i].sent = sock[i].recv = 0;
        // setNonblocking ( sock[i].fd );
				if (grace_conn && (i & 0xff) == 0xff)
					nanosleep(&for40ms, 0);
				clients.insert(clients_t::value_type(sock[i].fd, _client()));

        // fprintf ( stderr, "... socket handle created ... \n" );
    }

		gettimeofday(&tv2, 0);
		time_t diff = timeval_subtract(&tv2, &tv1);

		fprintf(stderr, "All %d sockets being connected in %lu usec, %lu usec per conn ", maxClients, diff, diff / maxClients);

		running = maxClients;
    //--------------------------------------------------
    // add all the sockets into epoll
    //--------------------------------------------------
    struct epoll_event ev;
    struct epoll_event* events = (epoll_event*)malloc(sizeof(ev) * maxClients);

    for ( i = 0; i < maxClients; i++ ) {
        memset ( &ev, 0, sizeof ev );
        ev.events = EPOLLIN | EPOLLOUT | EPOLLET;
        ev.data.fd = sock[i].fd;
        if ( epoll_ctl ( epfd, EPOLL_CTL_ADD, sock[i].fd, &ev ) < 0 ) {
            fprintf ( stderr, "epoll_ctl()\n" );
            exit ( 2 );
        }
    }

    //--------------------------------------------------------------
    // wait for the available status of each epoll descriptor
    //--------------------------------------------------------------
    int nfd;
		int sent = 0, failed = 0, reconnected = 0;
		
		if (reach_afer_conn)
			getchar();
			
    while ( !DONE ) {
        nfd = epoll_wait ( epfd, events, maxClients, 1000 );

        for ( i = 0; i < nfd; ++i ) {
						if (verbose)
            	fprintf ( stderr, "*** now polling *** i=%d\n", i );

            int client = events[i].data.fd;
            char message[1024];

						clients_t::iterator c = clients.find(client);
						assert(c != clients.end());
							
						if (!conn_only) {

	            if (events[i].events & EPOLLIN ) {
	                memset ( message, 0, sizeof message );
	                int read_len = read ( client, message, sizeof message );

									if (verbose)
	                	printf ( "read_len=%d\n", read_len );

									if (++c->second.recv == requests) {

		                // detached
		                epoll_ctl ( epfd, EPOLL_CTL_DEL, client, &ev );
		                // close socket
		                close ( client );
										--running;
										client = -1;


		                // add new descriptor
										if (RESPAWN_CLIENTS) {
											if (new_client(host, port, &ev, epfd)) {
												++running;
											}
											else {
												exit(3);
											}
										}
									}
							}

	            if (client != -1 && events[i].events & EPOLLOUT && c->second.recv == c->second.sent) {
	                sprintf ( message, "GET %s HTTP/1.1\r\nHost: %s:%d\r\n\r\n",
	                          requestURI, host, port );
	                int write_len = write ( client, message, strlen ( message ) );

									if (verbose)
	                	printf ( "write_len=%d\n", write_len );
									if ((++sent & 0xfff) == 0xfff) {
										printf("Sent %d requests; nfd=%d\n", sent, nfd);
									}
									++c->second.sent;
	            }								
						}


						if ((events[i].events & (EPOLLERR | EPOLLHUP)) != 0) {
							int result;
							socklen_t result_len = sizeof(result);
							getsockopt(events[i].data.fd, SOL_SOCKET, SO_ERROR, &result, &result_len);
							printf("ERROR on the fd [%d]: %d|%d\n", events[i].data.fd, errno, result);
              // detached
              epoll_ctl ( epfd, EPOLL_CTL_DEL, client, &ev );
              // close socket
              close ( client );
							--running;								
							++failed;		
							
							if (new_client(host, port, &ev, epfd)) {
								++running;
								++reconnected;
							}
							else {
								exit(3);
							}								
						}
								
						if (running == 0) {
							DONE = true;
							break;
						}
            

        }
    }

		typedef std::map<int,int>	stat_t;
		stat_t sentstat, recvstat;
		
		printf("STATS:\nRunning:\t%d\nSent requests:\t%d\nFailed conns:\t%d\nRespawned:\t%d\nCompleted conns:\t%d\n\n", running, sent, failed, reconnected, maxClients - running);
		
		if (sent != 0) {
			for (clients_t::const_iterator i = clients.begin(); i != clients.end(); ++i) {
			// 	printf("recv:%08d\tsent:%08d\n", i->second.first, i->second.second);
				recvstat[i->second.recv]++;
				sentstat[i->second.sent]++;
			}
		
			printf("\nRECV stat:\n");
			for (stat_t::const_iterator i = recvstat.begin(); i != recvstat.end(); ++i) {
				printf("%08d received %08d\n", i->second, i->first);
			}

			printf("\nSENT stat:\n");
			for (stat_t::const_iterator i = sentstat.begin(); i != sentstat.end(); ++i) {
				printf("%08d sent %08d\n", i->second, i->first);
			}
		}
				
		free(events);
		free(sock);

    return 0;
}

