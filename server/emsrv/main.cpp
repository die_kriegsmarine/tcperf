#include <project.h>
#include <eventmachine.h>
#include <iostream>
#include <sys/time.h>

namespace {
time_t timeval_subtract(struct timeval *t2, struct timeval *t1) {
    return (t2->tv_usec + 1000000 * t2->tv_sec) - (t1->tv_usec + 1000000 * t1->tv_sec);
}
}

// using namespace std;
// 
// class MyConnection: public EM::Connection {
//   public:
//     void ReceiveData (const char *data, int length) {
//       cout << data;
//       this->SendData(data, length);
//     }
// 
//     void PostInit() {
//       cout << "Post Init\n";
//     }
// 
//     void Unbind() {
//       cout << "Unbind\n";
//     }
// };
// 
// class MyAcceptor: public EM::Acceptor {
//   public:
//     MyConnection *MakeConnection() { 
//       cout << "Received new connection!\n";
//       return new MyConnection();
//     }  
// };
// 
// void start () {
//   EM::Acceptor *echo = new MyAcceptor();
//   echo->Start("127.0.0.1", 2202);
// }
// 
// int main() {
//   cout << "Starting the Reactor..\n";
//   EM::Run(start);
//   cout << "Finished Successfully!\n";
//   return 0
// }

int connections = 0;
int received = 0;
struct timeval /*tvBegin, tvEnd, tv1, */tv2;

// time_t tv_diff = timeval_subtract(&tv2, &tv1);
// unsigned int tv_total_diff = timeval_subtract(&tv1, &tvBegin) / 1000;
// memcpy(&tv1, &tv2, sizeof(tv1));


void emcb(const unsigned long binding, int state, const char* s, const unsigned long v3) {
	// printf("EM cb: %lu %d %s %lu\n", binding, state, s?s:"", v3);
	switch (state) {
		case EM_CONNECTION_ACCEPTED:
			if ((++connections & 0xfff) == 0xfff) {
				gettimeofday(&tv2, NULL);
				printf("[%lu] +client of %d\n", tv2.tv_sec, connections);				
			}
			break;
		case EM_CONNECTION_READ:
			{
				if ((++received & 0xffff) == 0xffff) {
					gettimeofday(&tv2, NULL);
					printf("[%lu] data received [%d], clients [%d]\n", tv2.tv_sec, received, connections);
				}
      	const char http_resp [] = "HTTP/1.0 200 OK\nContent-Length: 0\n\n";			
				EventableDescriptor *ed = dynamic_cast <EventableDescriptor*> (Bindable_t::GetObject (binding));
				if (ed)
					ed->SendOutboundData(http_resp, sizeof(http_resp));

				// evma_send_data_to_connection(binding, http_resp, sizeof(http_resp));			
			}
			break;
		case EM_CONNECTION_UNBOUND:
			if ((--connections & 0xfff) == 0xfff) {
				gettimeofday(&tv2, NULL);
				printf("[%lu] -client of %d\n", tv2.tv_sec, connections);				
			}
			break;
		default:
			break;
	}
}


int main(int argc, const char* argv[]) {
	
	EventMachine_t em(emcb);
	em.CreateTcpServer("0.0.0.0", argc > 1 ? atoi(argv[1]) : 8082);
	// em._UseKqueue();
	em._UseEpoll();
	em.Run();
	
	return 0;
}
