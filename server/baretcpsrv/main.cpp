/* For sockaddr_in */
#include <netinet/in.h>
/* For socket functions */
#include <sys/socket.h>
/* For fcntl */
#include <fcntl.h>
/* for select */
#include <sys/select.h>

#include <assert.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string>
#include <iostream>

#define MAX_LINE 16384

bool doubleLF[FD_SETSIZE];

int TOTAL_CONNECTED = 0;


char
rot13_char(char c)
{
	/* We don't want to use isalpha here; setting the locale would change
	 * which characters are considered alphabetical. */
	if ((c >= 'a' && c <= 'm') || (c >= 'A' && c <= 'M'))
		return c + 13;
	else if ((c >= 'n' && c <= 'z') || (c >= 'N' && c <= 'Z'))
		return c - 13;
	else
		return c;
}

struct fd_state {
	char buffer[MAX_LINE];
	size_t buffer_used;

	int writing;
	size_t n_written;
	size_t write_upto;
	std::string	input;
};

struct fd_state *
alloc_fd_state(void)
{
	struct fd_state *state = new fd_state;
	if (!state)
		return NULL;
	state->buffer_used = state->n_written = state->writing = state->write_upto = 0;
	return state;
}

void
free_fd_state(struct fd_state *state)
{
	delete(state);
}

void
make_nonblocking(int fd)
{
	fcntl(fd, F_SETFL, O_NONBLOCK);
}

int
do_read(int fd, struct fd_state *state)
{
	char buf[1024];
	int i;
	ssize_t result;

	while (1) {
		result = recv(fd, buf, sizeof(buf), 0);
		if (result <= 0)
			break;

		
//		std::cout << std::string(buf, result);
		state->input.append(buf, result);
		for (i = 0; i < result; ++i) {
			if (buf[i] == '\n' || buf[i] == '\r') {
				if (doubleLF[fd]) {
					std::string path;
					for (std::string::size_type cr = state->input.find('/'); cr != std::string::npos && state->input.c_str()[++cr] != ' ';) {
						path.insert(0, 1, state->input.c_str()[cr]);						
//						std::cout << state->input.c_str()[cr] << std::endl;
					}
					
					sprintf(state->buffer, "HTTP/1.1 200 OK\r\nContent-Length: %lu\r\nConnection: close\r\nContent-Type: text/html; charset=ISO-8859-1\r\n\r\nRequested:%s\n", 11+path.size(), path.c_str());
					state->buffer_used = strlen(state->buffer);
					doubleLF[fd] = false;
					state->writing = 1;
					state->write_upto = state->buffer_used;
					break;
				}else if (buf[i] == '\r') {
					doubleLF[fd] = true;
				}
			}else  {
				doubleLF[fd] = false;
			}
		}
	}

//	std::cout << std::endl;
	
	if (result == 0) {
		return 1;
	} else if (result < 0) {
		if (errno == EAGAIN)
			return 0;
		return -1;
	}

	return 0;
}

int
do_write(int fd, struct fd_state *state, bool* verbose)
{
	if (state->n_written == state->write_upto) {
		state->writing = -1;		
		return -1;
	}
	
	while (state->n_written < state->write_upto) {
		ssize_t result = send(fd, state->buffer + state->n_written,
				      state->write_upto - state->n_written, 0);
		if (result < 0) {
			if (errno == EAGAIN)
				return 0;
			return -1;
		}
		assert(result != 0);

		state->n_written += result;
	}

	if (state->n_written == state->buffer_used) {
		state->n_written = state->write_upto = state->buffer_used = 1;
//		state->writing = 2;
	}

	return 0;
}

void
run(void)
{
	int listener;
	struct fd_state **state = new fd_state*[FD_SETSIZE];
	struct sockaddr_in sin;
	int i, n, maxfd;
	fd_set readset, writeset, exset;

	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = 0;
	sin.sin_port = htons(8081);

	for (i = 0; i < FD_SETSIZE; ++i)
		state[i] = NULL;

	listener = socket(AF_INET, SOCK_STREAM, 0);
	make_nonblocking(listener);

#ifndef WIN32
	{
		int one = 1;
		setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one));
	}
#endif

	if (bind(listener, (struct sockaddr*)&sin, sizeof(sin)) < 0) {
		perror("bind");
		delete [] state;
		return;
	}

	if (listen(listener, 200) < 0) {
		perror("listen");
		delete [] state;
		return;
	}

	FD_ZERO(&readset);
	FD_ZERO(&writeset);
	FD_ZERO(&exset);
	

	while (1) {
		maxfd = listener;

		FD_ZERO(&readset);
		FD_ZERO(&writeset);
		FD_ZERO(&exset);

		FD_SET(listener, &readset);

		for (i = 0; i < FD_SETSIZE; ++i) {
			if (state[i]) {
				if (i > maxfd)
					maxfd = i;
				if (state[i]->writing == 0) {
					FD_SET(i, &readset);
				}
				if (state[i]->writing) {
					FD_SET(i, &writeset);
				}
			}
		}

		timeval timeout = {0, 1000 * 500};
		n = select(maxfd + 1, &readset, &writeset, &exset, &timeout);
		
		if (FD_ISSET(listener, &readset)) {
			struct sockaddr_storage ss;
			socklen_t slen = sizeof(ss);
			int fd = accept(listener, (struct sockaddr*)&ss, &slen);
			if (fd < 0) {
				perror("accept");
			} else if (fd > FD_SETSIZE) {
				close(fd);
			} else {
				printf("opened %d/%d\n", fd, ntohs(((struct sockaddr_in*)&ss)->sin_port));
				make_nonblocking(fd);
				state[fd] = alloc_fd_state();
				assert(state[fd]); /*XXX*/
				printf("Connected [%08d/%08d]\n", ++TOTAL_CONNECTED, fd);
			}
		}

		bool verbose = true;
		for (i = 0; i < maxfd + 1; ++i) {
			int r = 0;
			if (i == listener)
				continue;

			if (FD_ISSET(i, &readset)) {
				r = do_read(i, state[i]);
			}
			if (r == 0 && FD_ISSET(i, &writeset)) {
				r = do_write(i, state[i], &verbose);
			}
			if (state[i] && state[i]->writing == -1) {
				free_fd_state(state[i]);
				state[i] = NULL;
				close(i);
				printf("closed [%08d/%08d]\n", TOTAL_CONNECTED--, i);				
			}
		}
	}
	delete [] state;	
}

int
main(int c, char **v)
{
	printf("FD_SETSIZE=%d\n", FD_SETSIZE);
	setvbuf(stdout, NULL, _IONBF, 0);

	run();
	return 0;
}