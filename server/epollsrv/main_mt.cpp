#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <errno.h>
#include <sys/time.h>
#include <pthread.h>


#define MAXEVENTS 600000

namespace {
time_t timeval_subtract(struct timeval *t2, struct timeval *t1) {
    return (t2->tv_usec + 1000000 * t2->tv_sec) - (t1->tv_usec + 1000000 * t1->tv_sec);
}
}


struct _epoll_queue {
    int									m_listen_fd;
    int									m_epoll_queue_fd;
    struct epoll_event 	m_event;

};


static int
make_socket_non_blocking (int sfd) {
    int flags, s;

    flags = fcntl (sfd, F_GETFL, 0);
    if (flags == -1) {
        perror ("fcntl");
        return -1;
    }

    flags |= O_NONBLOCK;
    s = fcntl (sfd, F_SETFL, flags);
    if (s == -1) {
        perror ("fcntl");
        return -1;
    }

    return 0;
}

static int
create_and_bind (char *port) {
    struct addrinfo hints;
    struct addrinfo *result, *rp;
    int s, sfd;

    memset (&hints, 0, sizeof (struct addrinfo));
    hints.ai_family = AF_UNSPEC;     /* Return IPv4 and IPv6 choices */
    hints.ai_socktype = SOCK_STREAM; /* We want a TCP socket */
    hints.ai_flags = AI_PASSIVE;     /* All interfaces */

    s = getaddrinfo (NULL, port, &hints, &result);
    if (s != 0) {
        fprintf (stderr, "getaddrinfo: %s\n", gai_strerror (s));
        return -1;
    }

    for (rp = result; rp != NULL; rp = rp->ai_next) {
        sfd = socket (rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (sfd == -1)
            continue;

        make_socket_non_blocking(sfd);

        {
            int one = 1;
            setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one));
        }

        s = bind (sfd, rp->ai_addr, rp->ai_addrlen);
        if (s == 0) {
            /* We managed to bind successfully! */
            break;
        }

        close (sfd);
    }

    if (rp == NULL) {
        fprintf (stderr, "Could not bind\n");
        return -1;
    }

    freeaddrinfo (result);

    return sfd;
}

int conns = 0;
int served = 0, last_served = 0;
int connected = 0;
int sfd = 0;
int thread_fds[100];
int threads = 1;

void* thread_loop (void* thread_id_ptr) {

	long thread_id = reinterpret_cast<long>(thread_id_ptr);

	printf("Started thread %d\n", pthread_self());

    int efd = epoll_create (1024*600);
    if (efd == -1) {
        perror ("epoll_create");
        abort ();
    }

    struct epoll_event event;
    struct epoll_event *events;

    event.data.fd = sfd;
    event.events = EPOLLIN | EPOLLET;
    int s = epoll_ctl (efd, EPOLL_CTL_ADD, sfd, &event);
    if (s == -1) {
        perror ("epoll_ctl");
        abort ();
    }

    /* Buffer where events are returned */
    events = (epoll_event*)calloc (MAXEVENTS, sizeof event);

    struct timeval tvBegin, tvEnd, tv1, tv2;

    bool started = false;

    /* The event loop */
    while (1) {
        int n, i;

        n = epoll_wait (efd, events, MAXEVENTS, -1);
        #pragma omp parallel for
        for (i = 0; i < n; ++i) {
            if ((events[i].events & EPOLLERR) ||
                    (events[i].events & EPOLLHUP) ||
                    (!(events[i].events & EPOLLIN))) {
                /* An error has occured on this fd, or the socket is not
                   ready for reading (why were we notified then?) */
                fprintf (stderr, "epoll error: %d\n", (events[i].events & EPOLLHUP));
                close (events[i].data.fd);
                __sync_fetch_and_sub(&conns, 1);
                continue;
            }

            else if (sfd == events[i].data.fd) {
                /* We have a notification on the listening socket, which
                   means one or more incoming connections. */
                // for (int k = 0; k < 500000; ++k) {
                while (true) {
                    struct sockaddr in_addr;
                    socklen_t in_len;
                    int infd;
                    char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];
                    struct epoll_event event;


                    in_len = sizeof in_addr;
                    infd = accept (sfd, &in_addr, &in_len);
                    if (infd == -1) {
                        if ((errno == EAGAIN) ||
                                (errno == EWOULDBLOCK)) {
                            /* We have processed all incoming
                               connections. */
                            break;
                        } else {
                            perror ("accept");
                            break;
                        }
                    }

                    int s = getnameinfo (&in_addr, in_len,
                                         hbuf, sizeof hbuf,
                                         sbuf, sizeof sbuf,
                                         NI_NUMERICHOST | NI_NUMERICSERV);
                    if (s == 0) {
                        if (!started) {
                            #pragma omp critical
                            {
                                gettimeofday(&tvBegin, NULL);
                                memcpy(&tv1, &tvBegin, sizeof(tv1));
                                started = true;
                            }
                        }

                        int _connected = __sync_fetch_and_add(&connected, 1);

                        if ((_connected & 0xfff) == 0xfff) {
                            #pragma omp critical
                            {
                                gettimeofday(&tv2, NULL);
                                time_t tv_diff = timeval_subtract(&tv2, &tv1);
                                unsigned int tv_total_diff = timeval_subtract(&tv1, &tvBegin) / 1000;
                                memcpy(&tv1, &tv2, sizeof(tv1));
                                printf("[%08llu|%07u] Accepted connection on descriptor %d "
                                "(host=%s, port=%s)\n", tv_diff, tv_total_diff, infd, hbuf, sbuf);
																
																for (int k = 0; k < threads; ++k) {
																	printf("[%03d: conns: %d\n", k, thread_fds[k]);
																}
                            }
                        }
                    }

                    /* Make the incoming socket non-blocking and add it to the
                       list of fds to monitor. */
                    s = make_socket_non_blocking (infd);
                    if (s == -1)
                        abort ();

                    event.data.fd = infd;
                    event.events = EPOLLIN | EPOLLET;
                    s = epoll_ctl (efd, EPOLL_CTL_ADD, infd, &event);
										++thread_fds[thread_id];
                    if (s == -1) {
                        perror ("epoll_ctl");
                        abort ();
                    }
                    __sync_fetch_and_add(&conns, 1);
                }
                continue;
            } else {
                /* We have data on the fd waiting to be read. Read and
                   display it. We must read whatever data is available
                   completely, as we are running in edge-triggered mode
                   and won't get a notification again for the same
                   data. */
                int done = 0;
                int s = 0;

                while (1) {
                    ssize_t count;
                    char buf[512];

                    count = read (events[i].data.fd, buf, sizeof buf);
                    if (count == -1) {
                        /* If errno == EAGAIN, that means we have read all
                           data. So go back to the main loop. */
                        if (errno != EAGAIN) {
                            perror ("read");
                            done = 1;
                        }
                        break;
                    } else if (count == 0) {
                        /* End of file. The remote has closed the
                           connection. */
                        done = 1;
                        break;
                    }

                    /* Write the buffer to standard output */
                    //                   s = write (1, buf, count);
                    // sprintf(buf, "CONNS: %u\n", conns);
                    //                   s = write (1, buf, strlen(buf));
                    int _served = __sync_fetch_and_add(&served, 1);
                    if ((_served & 0xfffff) == 0xfffff) {
                        #pragma omp critical
                        {
		                        gettimeofday(&tv2, NULL);
                            s = write (1, buf, count);
														int passed = tv2.tv_sec - tv1.tv_sec;
                            sprintf(buf, "[%lu|%d rq/s] Served %d; CONNS: %u\n", tv2.tv_sec, (_served - last_served) / (passed != 0 ? passed : 1), _served, conns);
                            s = write (1, buf, strlen(buf));
														last_served = _served;
		                        memcpy(&tv1, &tv2, sizeof(tv1));
                        }
                    }
                    const char http_resp [] = "HTTP/1.0 200 OK\nContent-Length: 0\n\n";
                    s = write(events[i].data.fd, http_resp, strlen(http_resp));
                    if (s == -1) {
                        perror ("write");
                        abort ();
                    }
                }

                if (done) {
                  	int _conns = __sync_fetch_and_sub(&conns, 1);
                    if ((_conns & 0xfff) == 0xfff) {

                    	printf ("Closed connection on descriptor %d\n",
                            	events[i].data.fd);
										}

                    /* Closing the descriptor will make epoll remove it
                       from the set of descriptors which are monitored. */
                    close (events[i].data.fd);
                }
            }
        }
    }

    free (events);
		pthread_exit(0);
    return 0;
}

int
main (int argc, char *argv[]) {
    int s;

    if (argc < 2 && argc > 3) {
        fprintf (stderr, "Usage: %s [port]\n", argv[0]);
        exit (EXIT_FAILURE);
    }

		// pthread_init();
		threads = argc > 2 ? atoi(argv[2]) : 1;

    sfd = create_and_bind (argv[1]);
    if (sfd == -1)
        abort ();

    // s = make_socket_non_blocking (sfd);
    // if (s == -1)
    //     abort ();

    s = listen (sfd, SOMAXCONN);
    if (s == -1) {
        perror ("listen");
        abort ();
    }


		if (threads == 1)
			thread_loop((void*)sfd);
		else {
			pthread_t* pt = new pthread_t[threads];
			
			for (int i = 0; i < threads; ++i) {
				pthread_create(pt+i, 0, thread_loop, (void*)i);
			}

			for (int i = 0; i < threads; ++i) {
				pthread_join(*(pt+1), 0);
			}
			
			delete [] pt;
		}

    close (sfd);

    return EXIT_SUCCESS;
}
