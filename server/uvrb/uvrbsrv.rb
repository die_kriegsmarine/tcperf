require 'rubygems'
require 'bundler/setup'
require 'uvrb'

# @connected = 0
# @received = 0

class A
  
  @@instance = nil
  attr_accessor :connected, :received
  
  def connected
    @connected
  end
  
  def set_connected(v)
    @connected = v
  end

  def set_received(v)
    @received = v
  end  

  def received
    @received
  end
  
  def initialize
    @connected = 0
    @received = 0
  end

  def self.create
    @@instance = A.new
  end
  
  def self.instance
    @@instance
  end
    
end


def on_read(client, nread, buf)
  if nread == -1
    ptr = UV.last_error(@loop)
    p [UV.err_name(ptr), UV.strerror(ptr)]
    UV.close(client, proc {|client| })
    A::instance.connected -= 1
    if (A::instance.connected & 0xfff) == 0xfff
      puts "[#{Time.now.tv_sec} -client [#{A::instance.connected}]"
    end
    
  else
    A::instance.received += 1
    if (A::instance.received & 0x3fff) == 0x3fff
      puts "[#{Time.now.tv_sec} received [#{A::instance.received}]; conns [#{A::instance.connected}]"
    end
    
    puts buf[:base].read_string(nread)
  end
  UV.free(buf[:base])
end

def on_alloc(handle, suggested_size)
  UV.buf_init(UV.malloc(suggested_size), suggested_size)
end

def on_connect(server, status)
  client = FFI::AutoPointer.new(UV.create_handle(:uv_tcp), UV.method(:free))
  UV.tcp_init(@loop, client)

  UV.accept(server, client)
  UV.read_start(client, method(:on_alloc), method(:on_read))

  puts "A: #{A::instance.connected}"

  A::instance.connected += 1
  if (A::instance.connected & 0xfff) == 0xfff
    puts "[#{Time.now.tv_sec} +client [#{A::instance.connected}]"
  end
  
end

def main

  A::create

  puts "Listening on #{ARGV[0]} : #{A::instance.connected}"
  @loop = UV.default_loop

  server = FFI::AutoPointer.new(UV.create_handle(:uv_tcp), UV.method(:free))
  UV.tcp_init(@loop, server)

  UV.tcp_bind(server, UV.ip4_addr('0.0.0.0', ARGV[0].to_i))
  UV.listen(server, 128, method(:on_connect))
  
  UV.run(@loop)
  UV.loop_delete(@loop)
end

main
