///* For sockaddr_in */
#include <netinet/in.h>
///* For socket functions */
#include <sys/socket.h>
///* For fcntl */
#include <fcntl.h>
///* for select */
#include <sys/select.h>

#include <assert.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string>
#include <iostream>


int TOTAL_CONNECTED = 0;
int PORT = 8081;

void
make_nonblocking(int fd) {
	fcntl(fd, F_SETFL, O_NONBLOCK);
}

void
run(void) {
	int listener;
	struct sockaddr_in sin;
	int n, maxfd;
	fd_set readset, writeset, exset;
	
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = 0;
	sin.sin_port = htons(PORT);
	
	listener = socket(AF_INET, SOCK_STREAM, 0);
	make_nonblocking(listener);
	
#ifndef WIN32
	{
		int one = 1;
		setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one));
	}
#endif
	
	if (bind(listener, (struct sockaddr*)&sin, sizeof(sin)) < 0) {
		perror("bind");
		return;
	}
	
	if (listen(listener, 200) < 0) {
		perror("listen");
		return;
	}
	
	FD_ZERO(&readset);
	FD_ZERO(&writeset);
	FD_ZERO(&exset);
	
	bool	clients[FD_SETSIZE];
	memset(&clients, false, sizeof(clients));
	
	
	while (1) {
		maxfd = listener;
		
		FD_ZERO(&readset);
		FD_ZERO(&writeset);
		FD_ZERO(&exset);
		
		FD_SET(listener, &readset);
		
		for (int j = 0; j < FD_SETSIZE; ++j) {
			if (clients[j]) {
				if (maxfd < j)
					maxfd = j;
				FD_SET(j, &exset);
				FD_SET(j, &readset);
			}
		}
				
		timeval timeout = {0, 1000 * 500};
		n = select(maxfd + 1, &readset, &writeset, &exset, &timeout);
		
		if (n == -1) {
			printf("errno: %d\n", errno);
			continue;
		}
		
		if (FD_ISSET(listener, &readset)) {
			struct sockaddr_storage ss;
			socklen_t slen = sizeof(ss);
			int fd = accept(listener, (struct sockaddr*)&ss, &slen);
			if (fd < 0) {
				perror("accept");
			} else if (fd > FD_SETSIZE) {
				close(fd);
			} else {
				printf("opened %d/%d\n", fd, ntohs(((struct sockaddr_in*)&ss)->sin_port));
				make_nonblocking(fd);
				printf("Connected [%08d/%08d]\n", ++TOTAL_CONNECTED, fd);
				clients[fd] = true;
			}
		}
		
		for (int i = 0; i < maxfd + 1; ++i) {
			int r = 0;
			if (i == listener)
				continue;

			if (FD_ISSET(i, &exset)) {
				printf("Closing %d\n", i);
				close(i);
				clients[i] = false;
			}
			
			if (FD_ISSET(i, &readset)) {
				char b[100];
				n = read(i, b, 100);
				printf("Read %d\n", n);
				exit(2);
				// close(i);
				// clients[i] = false;
			}
			
		}
		
	}
}

int
main(int argc, const char* argv[]) {
	
	if (argc > 1)
		PORT = atoi(argv[1]);
	
	printf("FD_SETSIZE=%d; PORT: %d\n", FD_SETSIZE, PORT);
	setvbuf(stdout, NULL, _IONBF, 0);
	
	run();
	return 0;
}
